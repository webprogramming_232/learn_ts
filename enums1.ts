enum CadinalDirections {
    North = "North",
    East = "East",
    South = "South",
    West = "West",
}

let currentDirective = CadinalDirections.East;
console.log(currentDirective);